import { useParams } from "react-router-dom";
import NavBar from "../../custom components/NavBar";
import styles from "../../custom styles/styles";

function Dashboard() {

    const hotelname = useParams()["hotelname"];
    const password = useParams()["password"];

    return(
        <>
            <NavBar isHotel={true} />
            <div className="flex flex-col gap-10 p-5 justify-center items-center">
                <div className="leftNav bg-Seashell flex flex-row flex-wrap 
                gap-7 p-5 rounded-xl shadow-md shadow-SeashellLight
                items-center justify-around">
                    <button className={`hover:border-RedWood` + styles.CustomBorder}> Reservations </button>
                    <button className={`hover:border-RedWood` + styles.CustomBorder}> Reservations </button>
                    <button className={`hover:border-RedWood` + styles.CustomBorder}> Reservations </button>
                    <button className={`hover:border-RedWood` + styles.CustomBorder}> Reservations </button>

                </div>
                <div className="showData bg-Seashell w-full p-4 rounded-xl">
                    <div>reserefva room</div>
                    <div>reserefva room</div>
                    <div>reserefva room</div>
                    <div>reserefva room</div>
                    <div>reserefva room</div>
                    <div>reserefva room</div>
                    <div>reserefva room</div>
                    <div>reserefva room</div>
                    <div>reserefva room</div>
                    <div>reserefva room</div>
                </div>
            </div>
        </>
    );
}

export default Dashboard;